import logo from './logo.svg';
import './App.css';


// remove template in the module, to give way to the new line of code.

//when compiling multiple components, they should be wrapped inside a single element.


// to acquire the image you have to get the image fomr public folder. import the element inside this module.

//line 13 resource that was target was NOT saved in the public folder, so the file has to be imported
import anime from './luffy.jpg';
// pass down the alias that identifies the resource into out element.

function App() {
  return (
  <div>
    <h1>Welcome to the course booking batch 164</h1>
    <h5>This is our project in React</h5>
    <h6>Come Visit our Website</h6>

    {/*This image came from the /src folder*/}
    <img src={anime} width="500" height="500" alt="image not found" />

  {/*Lets try to display image from public folder*/}
  {/*import using public folder (direct import no need to declare variable)*/}
  <img src="/image/luffy.jpg" width="500" height="500"/>
    <h3>This is My Favorite Cartoon Character</h3>
  </div>
  );
}

export default App;
    
